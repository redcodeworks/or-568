# OR 568 Group Project

OR 568 is a graduate level Predictive Analytics course at George Mason University. This repository contains the project completed by Group 2 in the Spring 2022 semester.

## Team Roster

- Stavros Kalamatianos
- Pratibha Pandey
- Aleah Langrell
- Kevin Riley

## References
